// RadioLib library for LoRa
#include <RadioLib.h>
// ESP32 WiFi library
#include <WiFi.h>
// TaskScheduler library
#include <TaskScheduler.h>

// Declare task schedulers
void DataCallback();
void ForwardingCallback();
void HelloCallback();

// Initialize task schedulers and runner
// Task TaskName(repetition period (ms), number of times or TASK_FOREVER, &CallbackFunction)
Task DataTask(30000, TASK_FOREVER, &DataCallback);
Task ForwardingTask(1000, TASK_FOREVER, &ForwardingCallback);
Task HelloTask(10000, TASK_FOREVER, &HelloCallback);
Scheduler runner;

// TTGO T-Beam LoRa PINS
SX1276 radio = new Module(18, 26, 23, 33);

// LoRa settings
// - freq	          Carrier frequency in MHz. Allowed values range from
//                    137.0 MHz to 1020.0 MHz.
// - bw	            LoRa link bandwidth in kHz. Allowed values are 10.4, 15.6,
//                     20.8, 31.25, 41.7, 62.5, 125, 250 and 500 kHz.
// - sf	            LoRa link spreading factor. Allowed values range from 6
//                     to 12.
// - cr	            LoRa link coding rate denominator. Allowed values range
//                     from 5 to 8.
// - syncWord	      LoRa sync word. Can be used to distinguish different
//                    networks. Note that value 0x34 is reserved for
//                    LoRaWAN networks.
// - power	        Transmission output power in dBm. Allowed values range
//                    from 2 to 17 dBm.
// - preambleLength	Length of LoRa transmission preamble in symbols. The
//                    actual preamble length is 4.25 symbols longer than
//                    the set number. Allowed values range from 6 to 65535.
// - gain	          Gain of receiver LNA (low-noise amplifier). Can be set to
//                    any integer in range 1 to 6 where 1 is the highest gain.
//                    Set to 0 to enable automatic gain control (recommended).
#define FREQUENCY 868.1
#define BANDWIDTH 125
#define SF        7
#define CR        5
#define SYNCWORD  0x97
#define TXPOWER   2
#define PREAMBLE  16
#define AMPGAIN   0

// Routing table max size
#define RTMAXSIZE 256

struct routableNode
{
  byte address = 0;
  int metric;
  int lastSeqNo;
  unsigned long timeout;
  byte via;
};

// Routing table
routableNode routingTable[RTMAXSIZE];

// LoRa packets counter
int rxCounter = 0;
int rxDataCounter = 0;
int rxForwardCounter = 0;
int rxHelloCounter = 0;
int txCounter = 0;
int txDataCounter = 0;
int txForwardCounter = 0;
int txHelloCounter = 0;

// Duty cycle end
unsigned long dutyCycleEnd = 0;

// Time for last transmitted packet
unsigned long lastTxTime = 0;

// Routable node timeout (µs)
unsigned long routeTimeout = 10000000;

// LoRa broadcast address
byte broadcastAddress = 0xFF;

// LoRa local address
byte localAddress = 0x00;

// Metric
#define NO_FORWARDING                 0
#define FLOODING_BROADCAST_SINGLE_SF  1
#define SMART_BROADCAST_SINGLE_SF     2
#define HOP_COUNT_SINGLE_SF           3
#define RSSI_SUM_SINGLE_SF            4
#define RSSI_PROD_SINGLE_SF           5
#define ETX_SINGLE_SF                 6
#define TIME_ON_AIR_HC_CAD_SF        11
#define TIME_ON_AIR_SF_CAD_SF        12

int metric = HOP_COUNT_SINGLE_SF;

void initializeLocalAddress () {
  byte WiFiMAC[6];

  WiFi.macAddress(WiFiMAC);
  localAddress = WiFiMAC[5];

  Serial.print("Local LoRa address (from WiFi MAC): ");
  Serial.println(localAddress, HEX);
}

int routingTableSize() {
  int size = 0;

  for (int i = 0; i < RTMAXSIZE; i++) {
    if (routingTable[i].address != 0) {
      size++;
    }
  }
  return size;
}

void initializeLoRa () {
  Serial.println("LoRa module initialization...");
  
  int state = radio.begin(FREQUENCY, BANDWIDTH, SF, CR, SYNCWORD, TXPOWER, PREAMBLE, AMPGAIN);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }


  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }
  Serial.println("Done!");
  Serial.println("");
}


void setup() {

  // Serial monitor initialization
  Serial.begin(115200);
  while (!Serial);

  // LoRa initialization
  initializeLoRa();
  delay(1000);

  // Local address initialization
  initializeLocalAddress();

  delay(1000);

  // Scheduler initialization
  runner.init();

  runner.addTask(HelloTask);
  HelloTask.enable();

}

void sendHelloPacket() {

 // byte packetType = 0x04;

  Serial.print("Sending HELLO packet ");
  Serial.println(txHelloCounter);
  Serial.println();

  //Send LoRa packet to receiver
//  LoRa.beginPacket();                   // start packet
//  LoRa.write(broadcastAddress);         // add destination address
//  LoRa.write(localAddress);             // add sender address
//  LoRa.write(packetType);               // packet type
//  LoRa.write(helloCounter);             // add message ID

//  switch (metric) {
//    case HOPCOUNT:
//      for (int i = 0; i < routingTableSize(); i++) {
//        LoRa.write(routingTable[i].address);
//        LoRa.write(routingTable[i].metric);
//      }
//      break;
//    case RSSISUM:
//      break;
//  }

// LoRa.endPacket();                     // finish packet and send it


//  int state = radio.transmit("Hello World!");

  // you can also transmit byte array up to 256 bytes long
  /*
    byte byteArr[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF};
    int state = radio.transmit(byteArr, 8);
  */

  int state = radio.startTransmit((byte*) &txHelloCounter, sizeof(txHelloCounter));

  state++;

  txCounter++;
  txHelloCounter++;
}


void sendDataPacket() {

  byte packetType = 0x03;
  packetType++;
  // String outMessage = String(gps.location.lat(), 4) + String(gps.location.lng(), 4) + String(gps.altitude.feet() / 3.2808, 4);
  String outMessage = "AAAA";

  Serial.print("Sending DATA packet ");
  Serial.println(txDataCounter);
  Serial.println();

  // //Send LoRa packet to receiver
  // LoRa.beginPacket();                   // start packet
  // LoRa.write(broadcastAddress);         // add destination address
  // LoRa.write(localAddress);             // add sender address
  // LoRa.write(packetType);             // add message ID
  // LoRa.write(dataCounter);             // add message ID
  // //LoRa.write(outMessage.length());    // add payload length
  // LoRa.print(outMessage);               // add payload
  // LoRa.endPacket();                     // finish packet and send it

  

  int state = radio.startTransmit((byte*) &txDataCounter, sizeof(txDataCounter));
  state++;
  txCounter++;
  txDataCounter++;
}

bool isNodeInRoutingTable(byte address) {
  for (int i = 0; i < RTMAXSIZE; i++) {
    if ( routingTable[i].address == address) {
      return true;
    }
  }
  return false;
}

void processRoute(byte sender, int helloseqnum, int rssi, int snr, byte addr, int mtrc) {

  bool knownAddr = false;

  switch (metric) {
    case HOP_COUNT_SINGLE_SF:
      if (addr != localAddress) {
        for (int i = 0; i < routingTableSize(); i++) {
          if ( addr == routingTable[i].address) {
            knownAddr = true;
            if ( mtrc < routingTable[i].metric) {
              routingTable[i].metric = mtrc;
              routingTable[i].via = sender;
            }
            break;
          }
        }
        if (!knownAddr) {
          for (int i = 0; i < RTMAXSIZE; i++) {
            if ( routingTable[i].address == 0) {
              routingTable[i].address = addr;
              routingTable[i].metric = mtrc;
              routingTable[i].lastSeqNo = helloseqnum;
              routingTable[i].timeout = micros() + routeTimeout;
              routingTable[i].via = sender;
              Serial.println("New route added: " + String(addr, HEX) + " via " + String(sender, HEX) + " metric " + String(mtrc));
              break;
            }
          }
        }
      }
      break;
    default:
      break;
  }

  return;
}

void onReceive(int packetSize) {
  if (packetSize == 0)
    return;

  rxCounter++;

  //int rssi = LoRa.packetRssi();
  //int snr = LoRa.packetSnr();

  // Serial.print("Receiving LoRa packet ");
  // Serial.println(receivedPackets);
  // Serial.println("RSSI: " + String(rssi) + " dBm");
  // Serial.println("SNR: " + String(snr) + " dB");

  // read packet header bytes:
  //byte destination = LoRa.read();       // destination address
  //byte sender = LoRa.read();            // sender address
  //byte packetType = LoRa.read();        // packet type

  // if (destination == broadcastAddress) {
  //   if (packetType == 0x04) {
  //   //  int helloseqnum = LoRa.read();

  //     Serial.print("HELLO packet ");
  //     Serial.print(helloseqnum);
  //     Serial.print(" from 0x");
  //     Serial.print(sender, HEX);
  //     Serial.println(".");
  //     Serial.println("");

  //     switch (metric) {
        
  //       case HOPCOUNT:
  //         processRoute(localAddress, helloseqnum, rssi, snr, sender, 1);
  //         if (!isNodeInRoutingTable(sender)) {
  //           Serial.println("Adding new neighbour 0x" + String(sender, HEX) + " to the routing table.");
  //         }
  //         while (LoRa.available()) {
  //           byte addr = LoRa.read();
  //           int mtrc = LoRa.read();
  //           mtrc = mtrc + 1; // Add one hop to the received metric
  //           processRoute(sender, helloseqnum, rssi, snr, addr, mtrc);
  //         }
  //         Serial.println();
  //         //printRoutingTable();
  //         break;

  //       case RSSISUM:
  //         break;
  //     }
  //   }
  //   else if (packetType == 0x03) {
  //     Serial.println("Data broadcast message:");
  //     String inMessage = "";
  //     while (LoRa.available()) {
  //       inMessage += (char)LoRa.read();
  //     }
  //     Serial.print("PAYLOAD: ");
  //     Serial.println(inMessage);
  //     Serial.println("");
  //   }
  //   else {
  //     Serial.println("Random broadcast message... ignoring.");
  //   }
  // }

  // else if (destination == localAddress) {
  //   if ( packetType == 0x03 ) {
  //     Serial.print("Data packet from 0x");
  //     Serial.print(sender, HEX);
  //     Serial.print(" for me.");
  //   }
  //   else if ( packetType == 0x04) {
  //     Serial.print("HELLO packet from 0x");
  //     Serial.print(sender, HEX);
  //     Serial.print(" for me.");
  //   }
  // }

  // else {
  //   Serial.print("Packet from 0x");
  //   Serial.print(sender, HEX);
  //   Serial.print(" for 0x");
  //   Serial.print(destination, HEX);
  //   Serial.println(" (not for me). Ignoring.");
  //   Serial.println();
  // }
}

void addNeighborToRoutingTable(byte neighborAddress, int helloID) {

  for (int i = 0; i < RTMAXSIZE; i++) {
    if ( routingTable[i].address == 0) {
      routingTable[i].address = neighborAddress;
      routingTable[i].metric = 1;
      routingTable[i].lastSeqNo = helloID;
      routingTable[i].timeout = micros() + routeTimeout;
      routingTable[i].via = localAddress;
      Serial.println("New neighbor added in position " + String(i));
      break;
    }
  }
}

int knownNodes() {
  int known = 0;
  for (int i = 0; i < RTMAXSIZE; i++) {
    if ( routingTable[i].address != 0) {
      known++;
    }
  }
  return known;
}

void DataCallback() {
  Serial.print("DATA callback at t=");
  Serial.print(millis());
  Serial.println(" millis.");

  Serial.println(micros());
  Serial.println(dutyCycleEnd);

  if (dutyCycleEnd < millis()) {
    unsigned long transmissionStart = micros();

    sendDataPacket();

    unsigned long transmissionEnd = micros();

    unsigned long timeToNextPacket = 0;

    // Avoid micros() rollover
    if ( transmissionEnd < transmissionStart ) {
      timeToNextPacket = 99 * (timeToNextPacket - 1 - transmissionStart + transmissionEnd);
    }
    // Default behaviour
    else {
      timeToNextPacket = 99 * (micros() - transmissionStart);
    }

    dutyCycleEnd = millis() + timeToNextPacket / 1000 + 1;

    Serial.print("Scheduling next DATA packet in ");
    Serial.print(2 * timeToNextPacket / 1000);
    Serial.println(" ms.");

    HelloTask.setInterval(2 * (timeToNextPacket) / 1000);
  }
  Serial.println("");
}

void ForwardingCallback() {
  Serial.print("FORWARDING callback at t=");
  Serial.print(millis());
  Serial.println(" millis.");
}


void HelloCallback() {
  Serial.print("HELLO callback at t=");
  Serial.print(millis());
  Serial.println(" millis.");

  Serial.println(millis());
  Serial.println(dutyCycleEnd);

  if (dutyCycleEnd < micros()) {
    unsigned long transmissionStart = micros();

    sendHelloPacket();

    unsigned long transmissionEnd = micros();

    unsigned long timeToNextPacket = 0;

    // Avoid micros() rollover
    if ( transmissionEnd < transmissionStart ) {
      timeToNextPacket = 99 * (timeToNextPacket - 1 - transmissionStart + transmissionEnd);
    }
    // Default behaviour
    else {
      timeToNextPacket = 99 * (micros() - transmissionStart);
    }

    dutyCycleEnd = millis() + timeToNextPacket / 1000 + 1;

    Serial.print("Scheduling next HELLO packet in ");
    Serial.print(2 * timeToNextPacket / 1000);
    Serial.println(" ms.");

    HelloTask.setInterval(2 * (timeToNextPacket) / 1000);
  }

  Serial.println("");
}

void printRoutingTable() {
  Serial.println("Current routing table:");
  for (int i = 0; i < routingTableSize(); i++) {
    Serial.print(routingTable[i].address, HEX);
    Serial.print(" via ");
    Serial.print(routingTable[i].via, HEX);
    Serial.print(" metric ");
    Serial.println(routingTable[i].metric);
  }
  Serial.println("");
}

void loop() {
  runner.execute();

  // parse for a packet, and call onReceive with the result:
  //onReceive(LoRa.parsePacket());
}