# LoRaMesher

A hybird layer 2/3 routing protocol for LoRa mesh networks built with ESP32-based devices.

## License

This project is licensed under the [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)
